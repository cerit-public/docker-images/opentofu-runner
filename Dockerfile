FROM alpine:3

LABEL maintainer="Tomas Sapak <139890@muni.cz>"

ENV DEBIAN_FRONTEND=noninteractive

RUN export CRYPTOGRAPHY_DONT_BUILD_RUST=1\
    && apk add --no-cache --virtual build-dependencies python3-dev libffi-dev openssl-dev build-base unzip curl \
    && apk add openssh python3 git py3-pip py3-netaddr jq yq ansible \
    && ansible-galaxy collection install community.docker \
    && curl -LO https://github.com/opentofu/opentofu/releases/download/v1.7.2/tofu_1.7.2_amd64.apk \
    && apk add tofu_1.7.2_amd64.apk --allow-untrusted  \
    && rm tofu_1.7.2_amd64.apk \
    && apk del build-dependencies \
    && rm -rf /var/cache/apk/*

CMD ["/bin/sh"]
